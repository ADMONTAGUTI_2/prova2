package utfpr.ct.dainf.pratica;

import static java.lang.String.*;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 *
 * @author
 */
public class Ponto {

    private double x, y, z;

    /**
     * Retorna o nome não qualificado da classe.
     * @return O nome não qualificado da classe.
     */
    public String getNome() {
        return getClass().getSimpleName();
    }
    
    public Ponto() {
        this.x = 0;
        this.y = 0;
        this.z = 0;
    }
    
    public Ponto(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getZ() {
        return z;
    }

    public void setZ(double z) {
        this.z = z;
    }
    
    public double dist(Ponto p) {
        return Math.sqrt(Math.pow((p.x - this.x), 2) + Math.pow((p.y - this.y), 2) + Math.pow((p.z - this.z), 2));
    }

    @Override
    public String toString() {
        return this.getNome() + "("+ valueOf(this.x) + "," + valueOf(this.y) + "," + valueOf(this.z) + ")";
    }
    
    @Override
    public boolean equals (Object obj) {
        if (obj instanceof Ponto && this != null && obj != null) {
            Ponto p = (Ponto)obj;
            return this.x == p.x && this.y == p.y && this.z == p.z;
        }
        return false;
    }
}
