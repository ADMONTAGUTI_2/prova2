
import utfpr.ct.dainf.pratica.*;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * @author 
 */
public class Pratica {

    public static void main(String[] args) {
//        Ponto p = new Ponto();
//        System.out.println(p.toString());
          
//        Ponto p2 = new Ponto(10, 0, 1);
//        System.out.println(String.valueOf(p.dist(p2)));

        PontoXZ pontoXZ = new PontoXZ(-3,2);
        PontoXY pontoXY = new PontoXY(0,2);
        System.out.println("Distancia = " + pontoXZ.dist(pontoXY));
    }
}
